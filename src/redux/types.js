/**
 * we suggest using the "domain/action" convention for readability.
 */

/** Auth */
export const SET_USER = 'auth/login'
export const UNSET_USER = 'auth/reset'
export const SET_LOGIN_ERROR = 'auth/error'

/** Students */
export const SET_STUDENTS_LOADING = 'students/loading'
export const SET_STUDENTS_ERROR = 'students/error'
export const SET_STUDENTS = 'students/set'

export const SET_SELECTED_STUDENT_LOADING = 'students/selected/loading'
export const SET_SELECTED_STUDENT_ERROR = 'students/selected/error'
export const SET_SELECTED_STUDENT = 'students/selected/set'

/** Careers */
export const SET_CAREERS_LOADING = 'careers/loading'
export const SET_CAREERS_ERROR = 'careers/error'
export const SET_CAREERS = 'careers/set'