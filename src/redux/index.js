import { combineReducers } from 'redux';
import { authReducer } from './reducers/auth';
import { careersReducer } from './reducers/careers';
import { studentsReducer } from './reducers/students';
import { selectedStudentReducer } from './reducers/selectedStudent';

const rootReducer = combineReducers(
    {
        auth: authReducer,
        students: studentsReducer,
        selectedStudent: selectedStudentReducer,
        careers: careersReducer
    }
)

export default rootReducer;