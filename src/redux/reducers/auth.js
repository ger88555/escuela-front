import { SET_USER, UNSET_USER, SET_LOGIN_ERROR } from '../types'

const initialState = {
    user: {},
    authenticated: false,
    errors: {}
}

export const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_USER:
            return {
              ...state,
              user: action.payload,
              authenticated: true
            }
        case SET_LOGIN_ERROR:
          return {
            ...state,
            errors: {
              ...action.payload
            }
          }
        case UNSET_USER:
          return {
            user: {},
            authenticated: false,
            errors: {}
          }
        default:
            return state;
    }
}