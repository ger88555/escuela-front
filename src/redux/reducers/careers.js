import { SET_CAREERS, SET_CAREERS_LOADING, SET_CAREERS_ERROR } from '../types'

const initialState = {
    list: [],
    loading: false,
    error: null
}

export const careersReducer = (state = initialState, action) => {
    switch(action.type) {
      case SET_CAREERS: 
        return {
          ...state,
          list: [
            ...action.payload
          ],
          loading: false,
          error: null
      }

      case SET_CAREERS_LOADING: 
        return {
          ...state,
          list: [],
          loading: true,
          error: null
      }
      
      case SET_CAREERS_ERROR: 
        return {
          ...state,
          list: [],
          loading: false,
          error: action.payload
      }
  
      default:
        return state
    }
  }