import { SET_STUDENTS, SET_STUDENTS_LOADING, SET_STUDENTS_ERROR } from '../types'

const initialState = {
    list: [],
    loading: false,
    error: null
}

export const studentsReducer = (state = initialState, action) => {
    switch(action.type) {
      case SET_STUDENTS: 
        return {
          ...state,
          list: [
            ...action.payload
          ],
          loading: false,
          error: null
      }

      case SET_STUDENTS_LOADING: 
        return {
          ...state,
          list: [],
          loading: true,
          error: null
      }
      
      case SET_STUDENTS_ERROR: 
        return {
          ...state,
          list: [],
          loading: false,
          error: action.payload
      }
  
      default:
        return state
    }
  }