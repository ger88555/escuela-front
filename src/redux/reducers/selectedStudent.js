import { SET_SELECTED_STUDENT, SET_SELECTED_STUDENT_LOADING, SET_SELECTED_STUDENT_ERROR } from '../types'

const initialState = {
    data: {
      name: '',
      last_name: '',
      second_last_name: '',
      control_number: '',
      career: {
        id: null,
        name: ''
      }
    },
    loading: false,
    error: null
}

export const selectedStudentReducer = (state = initialState, action) => {
    switch(action.type) {
      case SET_SELECTED_STUDENT: 
        return {
          data: {
            ...action.payload
          },
          loading: false,
          error: null
      }

      case SET_SELECTED_STUDENT_LOADING: 
        return {
          data: {
            name: '',
            last_name: '',
            second_last_name: '',
            control_number: '',
            career: {
              id: null,
              name: ''
            }
          },
          loading: true,
          error: null
      }
      
      case SET_SELECTED_STUDENT_ERROR: 
        return {
          data: {
            name: '',
            last_name: '',
            second_last_name: '',
            control_number: '',
            career: {
              id: null,
              name: ''
            }
          },
          loading: false,
          error: action.payload
      }
  
      default:
        return state
    }
  }