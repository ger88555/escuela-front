import StudentsApi from '../../api/StudentsApi'
import { ApiException, ExpiredSessionException } from '../../api/exceptions'
import { handleExpiredAuthState } from './authActions'
import { SET_SELECTED_STUDENT, SET_SELECTED_STUDENT_LOADING, SET_SELECTED_STUDENT_ERROR } from '../types'

const setData = (payload) => ({
    type: SET_SELECTED_STUDENT,
    payload
});
  
const setLoading = () => ({
    type: SET_SELECTED_STUDENT_LOADING
});

const setError = (payload) => ({
    type: SET_SELECTED_STUDENT_ERROR,
    payload
})

export const getStudent = (controlNumber) => async dispatch => {
  await dispatch(setLoading())

  const api = new StudentsApi();

  await api.get(controlNumber)
    .then(response => {
      dispatch(setData(response.data))
    })
    .catch(error => {
        
      if (error instanceof ExpiredSessionException) {
        dispatch(handleExpiredAuthState());
        return;
      }

      const message = error instanceof ApiException ? error.message 
                                                    : 'Hubo un error al procesar la solicitud. Intente más tarde.' ;
      
      dispatch(setError(message))
  })
}