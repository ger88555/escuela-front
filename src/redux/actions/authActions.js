import { SET_USER, UNSET_USER, SET_LOGIN_ERROR } from '../types'
import { navigate } from "@reach/router";
import { setAuthorizationToken } from '../../utils/setAuthorizationToken'
import Swal from "sweetalert2";
import AuthApi from "../../api/AuthApi";
import { ApiException, ExpiredSessionException, ValidationException } from "../../api/exceptions";

const setUser = (payload) => ({
  type: SET_USER,
  payload
});

const clearUser = () => ({
  type: UNSET_USER
});

const setErrors = (payload) => ({
  type: SET_LOGIN_ERROR,
  payload
})


export const staffLogin = ({ rfc, password }, redirectTo) => {
  const api = new AuthApi();

  return processLoginResult( () => api.staffLogin(rfc, password), redirectTo );
};

export const studentLogin = ({control_number, password}, redirectTo ) => {
  const api = new AuthApi();

  return processLoginResult( () => api.studentLogin(control_number, password), redirectTo );
}

const processLoginResult = (loginCallback, redirectTo) => async (dispatch) => {
  await loginCallback()
    .then((data) => {
      window.sessionStorage.user = JSON.stringify(data.user);
      window.sessionStorage.token = data.token;

      setAuthorizationToken(data.token)
      dispatch(setUser(data.user));

    })
    .then(() => navigate(redirectTo))
    .catch((e) => dispatch(handleLoginError(e)));
}

/**
 * Handle an API login error.
 * 
 * @param {Error} e The error.
 */
const handleLoginError = (e) => async (dispatch) => {
  let errors = {};

  if (e instanceof ValidationException) {
    errors = e.errors;
  } else if (e instanceof ExpiredSessionException) {
    errors = { error: 'Credenciales incorrectas.' }
  } else if (e instanceof ApiException) {
    errors = { error: e.message };
  } else {
    errors = { error: 'Hubo un error al procesar la petición. Inténtelo más tarde.' }
  }

  dispatch(setErrors(errors));
}

export const restoreAuthenticatedUser = () => {
  return (dispatch) => {
    const user = sessionStorage.getItem('user')
    
    if(user) {
      dispatch(setUser(JSON.parse(user)))
    }

    return user;
  }
}

export const logout = (redirectTo) => {
  return async (dispatch, getState) => {
    const { user, authenticated } = getState('auth');
    
    try {
        if (authenticated) {
            const api = new AuthApi();

            'control_number' in user ? await api.studentLogout()
                                        : await api.staffLogout()
        }

        sessionStorage.clear()
        setAuthorizationToken(false)
        dispatch(clearUser())
        navigate(redirectTo)
    } catch (e) {
        if (e instanceof ExpiredSessionException) {
            dispatch(handleExpiredAuthState());
            navigate(redirectTo);
            return;
        }

        let errors;

        if (e instanceof ApiException) {
          errors = { error: e.message };
        } else {
          errors = { error: 'Hubo un error al procesar la petición. Inténtelo más tarde.' }
        }

        dispatch(setErrors(errors));
    }
  }
};

export const handleExpiredAuthState = () => {
  return dispatch => {
    dispatch(resetAuthState())

    Swal.fire({
      title: 'La sesión expiró',
      icon: 'error',
      confirmButtonColor: '#3085d6',
      confirmButtonText: 'Aceptar'
    })
  }
}

export const resetAuthState = () => {
  return dispatch => {
    dispatch(clearUser())
    sessionStorage.clear()
  }
}

/**
 * Restore the authentication data from session storage.
 * 
 * @returns {Promise}
 */
export const restoreAuthState = () => {
  return dispatch => {
    if(sessionStorage.token) {
      setAuthorizationToken( sessionStorage.getItem('token') )
      dispatch(setUser( JSON.parse( sessionStorage.getItem('user') ) ))
    } 
  }
}