import StudentsApi from '../../api/StudentsApi'
import { ApiException, ExpiredSessionException } from '../../api/exceptions'
import { handleExpiredAuthState } from './authActions'
import { SET_STUDENTS, SET_STUDENTS_LOADING, SET_STUDENTS_ERROR } from '../types'

const setList = (payload) => ({
    type: SET_STUDENTS,
    payload
});
  
const setLoading = () => ({
    type: SET_STUDENTS_LOADING
});

const setError = (payload) => ({
    type: SET_STUDENTS_ERROR,
    payload
})

export const getStudents = () => async dispatch => {
  await dispatch(setLoading())

  const api = new StudentsApi();

  await api.list()
    .then(response => {
      dispatch(setList(response.data))
    })
    .catch(error => {
        
      if (error instanceof ExpiredSessionException) {
        dispatch(handleExpiredAuthState());
        return;
      }

      const message = error instanceof ApiException ? error.message 
                                                    : 'Hubo un error al procesar la solicitud. Intente más tarde.' ;
      
      dispatch(setError(message))
  })
}