import CareersApi from '../../api/CareersApi'
import { ApiException, ExpiredSessionException } from '../../api/exceptions'
import { handleExpiredAuthState } from './authActions'
import { SET_CAREERS, SET_CAREERS_LOADING, SET_CAREERS_ERROR } from '../types'

const setList = (payload) => ({
    type: SET_CAREERS,
    payload
});
  
const setLoading = () => ({
    type: SET_CAREERS_LOADING
});

const setError = (payload) => ({
    type: SET_CAREERS_ERROR,
    payload
})

export const getCareers = () => async dispatch => {
  await dispatch(setLoading())

  const api = new CareersApi();

  await api.list()
    .then(response => {
      dispatch(setList(response.data))
    })
    .catch(error => {
        
      if (error instanceof ExpiredSessionException) {
        dispatch(handleExpiredAuthState());
        return;
      }

      const message = error instanceof ApiException ? error.message 
                                                    : 'Hubo un error al procesar la solicitud. Intente más tarde.' ;
      
      dispatch(setError(message))
  })
}