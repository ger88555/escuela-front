import React, { useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';

import  NavBar from '../../common/NavBar'
import { navigate, useLocation } from '@reach/router';

function App( props ) {
  const { pathname } = useLocation();

  useEffect(() => {
    pathname === '/' && navigate('/inicio');
  }, [pathname]);

  return (
    <div className='App'>
        <header className="App-header">
            <NavBar />
        </header>
        <div className='App-container'>
            {props.children}
        </div>
    </div>
  );
}

export default App;
