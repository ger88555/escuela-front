import React, { useEffect } from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import { connect } from 'react-redux'
import { getStudent } from '../../../redux/actions/selectedStudentActions'
import StudentForm from '../../common/StudentForm'

const UpdateStudent = ({ controlNumber, student, getStudent }) => {


  useEffect(() => {
    window.scrollTo(0, 0)
    getStudent(controlNumber)
  }, [])

  return(
    <Container className='my-3'>
      <Row>
        <Col>
          <h1>Actualizar Estudiante</h1>
        </Col>
      </Row>
      <hr/>
      <Row>
        <Col>
            <StudentForm studentData={student} />
        </Col>
      </Row>
    </Container>
  )
}

const mapStateToProps = state => {
    return {
        student: state.selectedStudent.data
    }
}

export default connect(mapStateToProps,{getStudent})(UpdateStudent)