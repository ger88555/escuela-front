import React from 'react'
import { navigate } from '@reach/router';
import { Container, Row, Col, Button } from 'react-bootstrap'
import StudentsTable from '../../common/StudentsTable'

const StudentsList = () => {
  return(
    <Container className='my-3'>
      <Row>
        <Col>
          <h1>Listado de Estudiantes</h1>
        </Col>
        <Col xs="auto">
          <Button variant="success" size="lg" title="Nuevo Estudiante" onClick={() => navigate('/estudiantes/nuevo')}>
            <i className="fa fa-user-plus"></i>
          </Button>
        </Col>
      </Row>
      <hr/>
      <Row>
        <Col>
          <StudentsTable />
        </Col>
      </Row>
    </Container>
  )
}

export default StudentsList