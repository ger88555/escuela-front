import StudentsList from './StudentsList'
import CreateStudent from './CreateStudent'
import UpdateStudent from './UpdateStudent'

export {
  StudentsList,
  CreateStudent,
  UpdateStudent
}