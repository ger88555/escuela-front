import React, { useEffect } from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import StudentForm from '../../common/StudentForm'

const CreateStudent = () => {

  useEffect(() => {
    window.scrollTo(0, 0)
  }, [])

  return (
    <Container className='my-3'>
      <Row>
        <Col>
          <h1>Registrar Estudiante</h1>
        </Col>
      </Row>
      <hr/>
      <Row>
        <Col>
          <StudentForm />
        </Col>
      </Row>
    </Container>
  )
}

export default CreateStudent