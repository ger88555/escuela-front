import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { Container, Row, Col } from 'react-bootstrap'

const Profile = ({ auth }) => {
  const { user } = auth;

  useEffect(() => {
    window.scrollTo(0, 0)
  }, [])

  return (
    <Container className='my-3'>
      <Row>
        <Col>
          <h1>Mi Perfil</h1>
        </Col>
      </Row>
      <hr/>
      <Row className="lead">
        <Col />

        <Col sm="10" md="8">
          <Row>
            <Col xs="4" className="font-weight-bold">Nombre</Col>
            <Col>{user.name} {user.last_name} {user.second_last_name}</Col>
          </Row>
          {'control_number' in user ? (
            <>
              <Row>
                <Col xs="4" className="font-weight-bold">Número de Control</Col>
                <Col>{user.control_number}</Col>
              </Row>
              <Row>
                <Col xs="4" className="font-weight-bold">Carrera</Col>
                <Col>{user.career.name}</Col>
              </Row>
            </>
          ) : (
            <Row>
              <Col xs="4" className="font-weight-bold">RFC</Col>
              <Col>{user.rfc}</Col>
            </Row>
          )}
        </Col>

        <Col />
      </Row>
    </Container>
  )
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  }
}

export default connect(mapStateToProps)(Profile)