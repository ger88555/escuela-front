import AuthCheck from './AuthCheck'
import StaffLogin from './StaffLogin'
import StudentLogin from './StudentLogin'

export {
  StaffLogin,
  StudentLogin,
  AuthCheck
}