import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { navigate } from '@reach/router'

const AuthCheck = ({ auth }) => {
  const { authenticated, user } = auth

  useEffect(() => {

    if (authenticated) {
      ('control_number' in user) ? navigate('/perfil') : navigate('/estudiantes');
    } else {
      navigate('/estudiantes/login');
    }

  }, [])
  
  return (
    <></>
  )
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  }
}

export default connect(mapStateToProps)(AuthCheck)