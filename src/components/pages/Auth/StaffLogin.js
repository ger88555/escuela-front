import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { Container, Row, Col, Card, Image } from 'react-bootstrap'
import LoginForm from '../../common/LoginForm'
import img from '../../../assets/images/staff.jpg'
import { staffLogin } from '../../../redux/actions/authActions'
import { navigate } from '@reach/router'

const StaffLogin = ({ auth, staffLogin }) => {
  const { authenticated, user, errors } = auth
  
  useEffect(() => {
    if (authenticated && 'rfc' in user) navigate('/estudiantes');
  }, [authenticated])

  return (
    <Container fluid>
      <Row className="vh-100 justify-content-center align-items-center">
        <Col
          xs={{span:10}}
          md={{span:10}}
          lg={{span: 8}}
        >
          <Card bg="light">
            <Card.Header>
              <h2>Instituto Tecnológico de Hermosillo</h2>
            </Card.Header>
            <Card.Body className="py-0 px-3">
              <Row className="justify-content-center align-items-center">
                <Col
                  xs={{span: 12}}
                  md={{span: 6}}
                  className="p-0"
                >
                  <Container fluid className="text-center px-0">
                    <Image src={img} fluid className="w-100" alt="Personal Administrativo" />
                  </Container>
                </Col>
                <Col
                  className="py-4"
                  xs={{span: 11}}
                  md={{span: 6}}
                >
                  <h3>Personal Administrativo</h3>
                  <hr/>
                    
                  <LoginForm
                    usernameLabel="RFC"
                    usernameField="rfc"
                    loginHandler={ staffLogin }
                    loginTo="/estudiantes"
                    errors={errors}
                  />
                </Col>
              </Row>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  )
}

export default connect(state => ({ auth: state.auth }), {staffLogin})(StaffLogin)