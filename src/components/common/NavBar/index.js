import React from 'react'
import './NavBar.css'
/** redux */
import { connect } from 'react-redux'
import { logout } from '../../../redux/actions/authActions'

import { Navbar, Nav, NavDropdown } from 'react-bootstrap'

const NavBar = ({ auth, logout }) => {
  const { user } = auth;

  const logoutHandler = () => {

    'control_number' in user
      ? logout('/estudiantes/login')
      : logout('/personal/login')
  }

  return (
    <Navbar bg="dark" variant="dark" expand="lg">

        <Navbar.Brand href="#"> <h3>Sistema Escolar</h3> </Navbar.Brand>

        <Nav className="mr-auto">
          {'rfc' in user && (
            <Nav.Link href="/estudiantes">
                <i className="fas fa-user-graduate"></i>
                Estudiantes
            </Nav.Link>
          )}
        </Nav>

        <Nav>
            <NavDropdown id="collasible-nav-dropdown" title={`${user.name} ${user.last_name} ${user.second_last_name}`} alignRight>
                <NavDropdown.Item href="/perfil">Mi perfil</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item onClick={logoutHandler} >Cerrar sesion</NavDropdown.Item>
            </NavDropdown>
        </Nav>

    </Navbar>
  )
}

/** This function is passed as parameter to connect function. 
 * Its function is to return the value from the state that you should
 * recive as props in the NavBar Component   */
const mapStateToProps = state => {
  return {
      auth: state.auth
    }
}

/** Connect function allow us access to the global state */
export default connect(mapStateToProps,{ logout })(NavBar)