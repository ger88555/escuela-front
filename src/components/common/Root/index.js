import React from 'react'
import { Router } from '@reach/router'
import { connect } from 'react-redux'
import App from '../../layouts/App'
import { AuthCheck, StaffLogin, StudentLogin } from '../../pages/Auth';
import { PrivateRoute } from '../PrivateRoute'
import routes from '../../../routes'

/**
 * Routes.
 */
const Root = ({auth}) => {
  return (
    <Router>
      <StaffLogin path="/personal/login" />
      <StudentLogin path="/estudiantes/login" />
      <App path="/">
        <AuthCheck path="inicio" {...auth} />
        {routes.map((route) => (<PrivateRoute {...route} {...auth} />))}
      </App>
    </Router>
  )
}

/** This function is passed as parameter to connect function. 
 * Its function is to return the value from the state that you should
 * recive as props in the Root Component   */
const mapStateToProps = state => {
  return {
      auth: state.auth
    }
}
/** Connect function allow us access to the global state */
export default connect(mapStateToProps)(Root)