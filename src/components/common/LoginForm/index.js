import React from "react";
import { useForm } from 'react-hook-form';
import { Button, Form, Alert, Col } from "react-bootstrap";

const LoginForm = ({usernameLabel, usernameField, loginHandler, loginTo, errors}) => {
  const { register, handleSubmit } = useForm();

  const onSubmit = (data, event) => {
    event.preventDefault()
    loginHandler(data, loginTo);
  }

  return (
    <Form onSubmit={ handleSubmit(onSubmit) }>
      <Form.Group>
        <Form.Label>{usernameLabel}</Form.Label>
        <Form.Control type="text" autoFocus {...register(usernameField, { required: true})}/>
      </Form.Group>
      {errors[usernameField] && <Alert variant='danger'>{ errors[usernameField][0] }</Alert>}

      <Form.Group>
        <Form.Label>Contraseña</Form.Label>
        <Form.Control type="password" {...register('password', { required: true})}/>
      </Form.Group>
      {errors.password && <Alert variant='danger'>{ errors.password[0] }</Alert>}
      { errors.error && <Alert variant='danger'>{ errors.error }</Alert>}
      
      <Form.Row className="justify-content-end">
          <Col xs="auto">
            <Button type='submit'>Iniciar sesión</Button>
          </Col>
      </Form.Row>
    </Form>
  );
};

export default LoginForm;
