import React, {useCallback, useEffect} from "react"
import { Form, Button, Alert } from 'react-bootstrap'
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
import { useForm, FormProvider } from 'react-hook-form'
import CareerSelector from '../CareerSelector'
import { navigate } from "@reach/router"
import StudentsApi from "../../../api/StudentsApi"
import { ApiException, ExpiredSessionException, ValidationException } from "../../../api/exceptions"
import { handleExpiredAuthState } from '../../../redux/actions/authActions'
import { connect } from "react-redux"

const StudentForm = ({ studentData, handleExpiredAuthState }) => {

  const recordExists = useCallback(() => (studentData?.control_number), [studentData?.control_number]);

  /** Serialize `studentData` to the Form fields */
  const serializeData = () => {
    if( studentData && Object.keys(studentData).length ){
      console.log(studentData)
      const { name, last_name, second_last_name, career: { id: career_id }, control_number} = studentData
      return {
        name,
        last_name,
        second_last_name,
        career_id,
        control_number
      }
    }
  }
  const formMethods = useForm()
  const { register, handleSubmit, formState: { errors }, reset } = formMethods

  const alert = withReactContent(Swal)
  
  const onSubmit = data => {
    const api = new StudentsApi();

    (recordExists() ? api.update(studentData.control_number, data) : api.store(data))
      .then( response => {
        alert.fire({
          title: 'Exito',
          text: 'El alumno se dio de alta correctamente',
          icon: 'success'
        })
        navigate('/estudiantes')
      })
      .catch( error => {
        if (error instanceof ExpiredSessionException) {
          return handleExpiredAuthState();
        }

        let message = 'Hubo un error al enviar la solicitud al servidor.';

        if (error instanceof ValidationException) {
          message = Object.values(error.errors).join(' \n');
        } else if (error instanceof ApiException) {
          message = error.message;
        }

        alert.fire({
          title: 'Error',
          text: message,
          icon: 'error'
        })
      })
  }

  useEffect( () => {
    window.scrollTo(0, 0)
    /** set data when is update form */
    reset(serializeData())
  }, [studentData])

  return (
    <FormProvider {...formMethods}>
      
        <Form onSubmit={handleSubmit(onSubmit)} >
          <Form.Group>
              <Form.Label>Nombre</Form.Label>
              <Form.Control
                  id="name"
                  type="text"
                  autoFocus
                  {...register('name', {
                    required: 'El campo de nombre es obligatorio.'
                  })}
              />
              {errors.name && <Alert variant='danger' className='mt-2'>{errors.name.message}</Alert>}
          </Form.Group>
          <Form.Group>
              <Form.Label>Primer Apellido</Form.Label>
              <Form.Control
                  id="last_name"
                  type="text"
                  {...register('last_name', {
                    required: 'El campo de primer apellido es obligatorio.'
                  })}
              />
              {errors.last_name && <Alert variant='danger' className='mt-2'>{errors.last_name.message}</Alert>}
          </Form.Group>
          <Form.Group>
              <Form.Label>Apellido Materno</Form.Label>
              <Form.Control
                  id="second_last_name"
                  type="text"
                  {...register('second_last_name', {
                    required: 'El campo de segundo apellido es obligatorio.'
                  })}
              />
              {errors.second_last_name && <Alert variant='danger' className='mt-2'>{errors.second_last_name.message}</Alert>}
          </Form.Group>
          <Form.Group>
              <Form.Label>Carrera</Form.Label>
              <CareerSelector />
              {errors.career_id && <Alert variant='danger' className='mt-2'>{errors.career_id.message}</Alert>}
          </Form.Group>
          <Form.Group>
              <Form.Label>Número de control</Form.Label>
              <Form.Control 
                  id="control_number"
                  type="text"
                  {...register('control_number', { required: 'El campo de número de control es obligatorio.' })}
              />
              {errors.control_number && <Alert variant='danger' className='mt-2'>{errors.control_number.message}</Alert>}
          </Form.Group>
          <Form.Group>
              <Form.Label>Contraseña</Form.Label>
              <Form.Control 
                  id="password"
                  type="password"
                  {...register('password', { required: ( recordExists() ) ? undefined  : 'El campo de contraseña es obligatorio.' })} 
              />
              {errors.password && <Alert variant='danger' className='mt-2'>{errors.password.message}</Alert>}
          </Form.Group>
          <Form.Group>
              <Form.Label>Confirmación de Contraseña</Form.Label>
              <Form.Control 
                  id="password_confirmation"
                  type="password"
                  {...register('password_confirmation', { required: ( recordExists() ) ? undefined : 'El campo de confirmación de contraseña es obligatorio.' })} 
              />
              {errors.password_confirmation && <Alert variant='danger' className='mt-2'>{errors.password_confirmation.message}</Alert>}
          </Form.Group>
          <Button type='submit' variant="primary">
              Guardar
          </Button>
      </Form>

    </FormProvider>  
  )
}

export default connect(null, {handleExpiredAuthState})(StudentForm)
