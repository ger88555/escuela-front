import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { getCareers } from '../../../redux/actions/careerActions'
import { Form } from 'react-bootstrap'
import { useFormContext } from 'react-hook-form'

const CareerSelector = ({ getCareers, careers }) => {

  const { register } = useFormContext();

  useEffect( () => {
    !careers.length && getCareers()
   }, [])

   return (
     <Form.Control as="select" id="career_id" {...register('career_id', { required: 'El campo de carrera es obligatorio.' })}>
        <option value=''>SELECCIONAR CARRERA</option>
        {
          careers.map( career => <option value={career.id}>{career.name}</option> )
        }
     </Form.Control>
   )
}

export default connect((state) => ({ careers: state.careers.list }),{ getCareers })(CareerSelector)