import React from 'react'
import { Redirect } from '@reach/router'
import { STUDENT, STAFF } from '../../../constants/userTypes'

export const PrivateRoute = ({ component : Component, user, authenticated, type, ...rest }) => {
  if (!authenticated) {
    return <Redirect from="/" to="/estudiantes/login" noThrow replace />
  }

  if (type === STUDENT && user?.control_number === undefined) {
    return <Redirect from="/" to="/estudiantes/login" noThrow />
  }

  if (type === STAFF && user?.rfc === undefined) {
    return <Redirect from="/" to="/personal/login" noThrow />
  }

  return <Component {...rest}/>
}