import React, { useEffect } from 'react'
import { navigate } from '@reach/router';
import { Table, Button } from 'react-bootstrap'
import { connect } from 'react-redux'
import { getStudents } from '../../../redux/actions/studentsActions'
import StudentsApi from '../../../api/StudentsApi';

import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
import { ApiException, ExpiredSessionException } from '../../../api/exceptions';
import { handleExpiredAuthState } from '../../../redux/actions/authActions'

const StudentsTable = ({ students, loading, error, getStudents, handleExpiredAuthState }) => {

  useEffect(() => {
    getStudents();
  }, []);

  const alert = withReactContent(Swal)

  const onDelete = (control_number) => {
      const api = new StudentsApi();

      api.delete(control_number).then(response => {
        alert.fire({
          title: 'Alumno eliminado.',
          text: 'El alumno con número de control ' + control_number + ' ha sido eliminado.'
        });

        getStudents();
      }).catch(error => {
        if (error instanceof ExpiredSessionException) {
            return handleExpiredAuthState();
        }

        let message = 'Hubo un error al procesar la solicitud.';

        if (error instanceof ApiException) {
            message = error.message
        }

        alert.fire({
          title: 'Hubo un error al intentar eliminar el estudiante.',
          text: message
        });
      })
  }

  return(
    <Table striped bordered hover>
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Número de Control</th>
                <th>Carrera</th>
                <th>Acción</th>
            </tr>
        </thead>
        <tbody>
            {loading ? (
                <tr>
                    <td colSpan="3" className="text-center">Cargando...</td>
                </tr>
            ) : (

                error ? (
                    <tr>
                        <td colSpan="3" className="text-center">
                            {error}
                            <Button onClick={getStudents}>Reintentar <i className="fa fa-repeat" /> </Button>
                        </td>
                    </tr>
                ) : (

                    students.length === 0 ? (
                        <tr>
                            <td colSpan="3" className="text-center">Sin registros.</td>
                        </tr>
                    ) : (

                        students.map((student) => (
                            <tr>
                                <td>{student.name} {student.last_name} {student.second_last_name}</td>
                                <td>{student.control_number}</td>
                                <td>{student.career.name}</td>
                                <td>
                                    <Button onClick={() => navigate('/estudiantes/' + student.control_number)}>
                                        <i className="fa fa-pencil"/>
                                    </Button>
                                    <Button variant="danger" onClick={() => onDelete(student.control_number)}>
                                        <i className="fa fa-trash"/>
                                    </Button>
                                </td>
                            </tr>
                        ))

                    )
                )

            )}              
        </tbody>
    </Table>
  )
}

const mapStateToProps = state => {
    return {
        students: state.students.list,
        loading: state.students.loading,
        error: state.students.error
    }
}

export default connect(mapStateToProps,{ getStudents, handleExpiredAuthState })(StudentsTable)