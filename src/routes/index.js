import { CreateStudent, StudentsList, UpdateStudent } from "../components/pages/Students";
import Profile from "../components/pages/Profile";
import { STUDENT, STAFF } from "../constants/userTypes";

const routes = [
    {
        path: 'perfil',
        component: Profile,
    },
    {
        path: 'estudiantes',
        component: StudentsList,
        type: STAFF
    },
    {
        path: 'estudiantes/:controlNumber',
        component: UpdateStudent,
        type: STAFF
    },
    {
        path: 'estudiantes/nuevo',
        component: CreateStudent,
        type: STAFF
    },
]

export default routes;