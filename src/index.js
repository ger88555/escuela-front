import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux'
import Root from './components/common/Root';
import reportWebVitals from './reportWebVitals';
import thunk from 'redux-thunk'
import rootReducer from './redux'
import { restoreAuthState } from './redux/actions/authActions';
import './index.css';


const store = createStore(rootReducer, {}, applyMiddleware(thunk))

store.dispatch(restoreAuthState())

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Root />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
