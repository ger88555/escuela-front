import Api  from './Api'
import axios from 'axios'

class AuthApi extends Api {
    async staffLogin(username, password){
        const request = async () => await axios.post(this.generateAbsolutePath('auth/staff/login'), {
            rfc: username,
            password
        });
            
        const { data } = await this.perform(request);

        return data;
    }

    async staffInfo(){
        const request = async () => await axios.get(this.generateAbsolutePath('auth/staff/me'));

        const { data } = await this.perform(request);

        return data;
    }

    async staffLogout(){
        const request = async () => await axios.post(this.generateAbsolutePath('auth/staff/logout'));
            
        const { data } = await this.perform(request);

        return data;
    }



    async studentLogin(username, password){
        const request = async () => await axios.post(this.generateAbsolutePath('auth/student/login'), {
            control_number: username,
            password
        });

        const { data } = await this.perform(request);

        return data;
    }

    async studentInfo(){
        const request = async () => await axios.get(this.generateAbsolutePath('auth/student/me'));

        const { data } = await this.perform(request);

        return data;
    }

    async studentLogout(){
        const request = async () => await axios.post(this.generateAbsolutePath('auth/student/logout'));
            
        const { data } = await this.perform(request);

        return data;
    }
}

export default AuthApi;