import Api  from './Api'
import axios from 'axios'

class StudentsApi extends Api {
    async list(){
        const request = async () => await axios.get(this.generateAbsolutePath('students'));
            
        const { data } = await this.perform(request);

        return data;
    }

    async store(data){
        const request = async () => await axios.post(this.generateAbsolutePath('students'), this.filterData(data));

        return await this.perform(request);
    }

    async get(controlNumber){
        const request = async () => await axios.get(this.generateAbsolutePath(`students/${controlNumber}`));

        const { data } = await this.perform(request);

        return data;
    }

    async update(controlNumber, data){
        const request = async () => await axios.put(this.generateAbsolutePath(`students/${controlNumber}`), this.filterData(data));

        return await this.perform(request);
    }

    async delete(controlNumber){
        const request = async () => await axios.delete(this.generateAbsolutePath(`students/${controlNumber}`));

        await this.perform(request);
    }

    /**
     * Filter the form data for the API call.
     * @param {Object} data Form fields.
     * @returns 
     */
    filterData(data){

        if (!data.password) {
            delete data.password
            delete data.password_confirmation
        }

        return data;
    }
}

export default StudentsApi;