import Api  from './Api'
import axios from 'axios'

class CareersApi extends Api {
    async list(){
        const request = async () => await axios.get(this.generateAbsolutePath('careers'));
            
        const { data } = await this.perform(request);

        return data;
    }
}

export default CareersApi;